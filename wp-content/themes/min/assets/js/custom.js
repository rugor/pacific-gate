/**
 * scripts.js
 */
jQuery.fn.reverse = [].reverse;

$(document).ready(function(){

	var container = $("#container");

	var getUnits = function() {
		$.ajax({
			type: "GET",
			url: "http://marcastudio.com/app/platform/XML/Pacific_Gate_availability.xml",
			datatype: "xml",
			error: function(jqXHR, textStatus, errorThrown) {
				console.log('Error: ' + errorThrown);
			},
			success: function(xml) {
				console.log('AJAX request succeeded.');

				var unitsAvail = '';

				$(xml).find('byfloor').children().reverse().each(function(){

					unitsAvail += '<div class="row">';
					
					var $this = $(this);
					
					$this.children().each(function(){

						// var status = this.node.find('status').text();
						var status = $(this).find('status').text();

						unitsAvail += '<div class="cell ' + status + '"' + '>';
						
						var floorNum = this.parentNode.nodeName.replace('floor','');;
						var unitNum = this.nodeName.replace('unit','');

						unitsAvail += floorNum;
						unitsAvail += unitNum;
				        unitsAvail += '</div>';
				    });
					
					unitsAvail += '</div>';

				});

				var estate = '<div class="row quality"><div class="cell">Estate</div></div>'
				var luxury ='<div class="row quality"><div class="cell">Luxury</div>'
				
				container.append(unitsAvail).hide();
				container.find(".row").eq(0).before(estate);
				container.find(".row").eq(10).before(luxury);
				container.fadeIn(1000);
			}
		});
	};
	
	getUnits();

	setInterval(function() {
		
		container.fadeOut(1000, function(){
			container.delay(800).empty();
			getUnits();
		});

	}, 1000 * 60 * .5); // where X is your every X minutes
		
});