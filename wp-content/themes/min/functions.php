<?php
register_nav_menu( 'primary', 'Main Menu' );
show_admin_bar(false);

/**
* 	Works in WordPress 4.1 or later.
*/
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}
	
/**
 * Proper way to enqueue scripts and styles
 */
function custom_scripts() {
	wp_enqueue_style( 'style-main', get_template_directory_uri() . '/style.css' );
	/// wp_enqueue_script( 'plugin-script', get_template_directory_uri() . '/assets/js/vendors.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/custom.min.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'custom_scripts' );

// ###########################
// Remove [...]
// ###########################

function new_excerpt_more( $more ) {
	return '....';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// remove emoji script in head
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_action('wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');

function disable_embeds_init() {

    // Remove the REST API endpoint.
    // remove_action('rest_api_init', 'wp_oembed_register_route');

    // Turn off oEmbed auto discovery.
    // Don't filter oEmbed results.
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

    // Remove oEmbed discovery links.
    remove_action('wp_head', 'wp_oembed_add_discovery_links');

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action('wp_head', 'wp_oembed_add_host_js');
}

add_action('init', 'disable_embeds_init', 9999);
?>